<?php
/**
 * Copyright © Push ON All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'PushON_AmastyRulesCache', __DIR__);

