<?php
/**
 * Copyright © Push ON All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PushON\AmastyRulesCache\Plugin\Amasty\RulesPro\Model\Rule\Condition;

class Customer
{   
    private static $attributeCache = [];     
    public function aroundGetInputType(
        \Amasty\RulesPro\Model\Rule\Condition\Customer $subject,
        \Closure $proceed
    ) {  
        $attributeCode = $subject->getAttribute();

        if (!empty(self::$attributeCache[$attributeCode])) {
            return self::$attributeCache[$attributeCode];
        }
        $returnValue = $proceed();
        self::$attributeCache[$attributeCode] = $returnValue;
            
        return self::$attributeCache[$attributeCode];
    }
}

